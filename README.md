**Introduction:**
To automate the configuration management of infrastructure resources provisioned using Terraform using Puppet.

**Prerequisties:**
1. Terraform installed on your local machine
2. Puppetmaster running either in a VM or AWS EC2 instance

**Terraform:**

- Navigate to the Terraform directory.
- Initialize Terraform with terraform init.
- Validate the configuration with terraform validate.
- Review the execution plan with terraform plan.
- Apply changes with terraform apply


**Puppet server setup:**
 
- Manually set up and configure the Puppet Server on a AWS EC2 instance.
